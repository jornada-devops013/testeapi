FROM mcr.microsoft.com/dotnet/sdk:8.0
WORKDIR /app

# Copy everything
COPY . ./
EXPOSE 80
# Restore as distinct layers
RUN dotnet restore
# Build and publish a release
RUN dotnet publish -c Release -o out
